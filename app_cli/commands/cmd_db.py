import click

from sqlalchemy_utils import database_exists, create_database , drop_database
from sqlalchemy import create_engine
from sqlalchemy import inspect

from server_app import create_app
from server_app_extensions import db

from blueprints.track.model import Track


# Create an app context for the database connection.
app = create_app()
db.app = app


@click.group()
def cli():
    """ Run Database related tasks. """
    pass


@cli.command()
def init_db():
    """Clear database and then create all new tables."""

    db_uri = app.config['SQLALCHEMY_DATABASE_URI']
    if not database_exists(db_uri):
        create_database(db_uri)

    db.drop_all()
    db.create_all()

    click.echo('')
    click.echo('-----------------------')
    click.echo('')
    print ("Database Initialized")
    click.echo('')
    click.echo('-----------------------')
    click.echo('')

    return None

cli.add_command(init_db)
