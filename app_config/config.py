ACTIVATE_DEBUG = False         # In Development best to leave it as True.
LOG_TO_FILE = True              # This will save logs to file defined in app_library\lib_logger
LOG_TO_CONSOLE = True           # This will display logs on console as defined in app_library\lib_logger

SECRET_KEY = 'insecurekeyfordev'

SERVER_NAME = 'localhost'

# SQLAlchemy.

#MYSQL Docker Contianer
db_uri = 'mysql://root:abc123@mysql_srv:3306/track_db'

#SQLITE
#db_uri = 'sqlite:///track_db.db'

#POSTGRESS Docker Contianer
#db_uri = 'postgresql://postgres:abc123@localhost:5432/track_db'

SQLALCHEMY_DATABASE_URI = db_uri
SQLALCHEMY_TRACK_MODIFICATIONS = False
