# Ref > https://stackoverflow.com/questions/38178776/function-object-has-no-attribute-name-when-registering-blueprint

from flask import Blueprint
from flask import Response
health = Blueprint('health', __name__)


@health.route("/health", methods=["GET"])
def health():
    jd = {'status': 'OK'}
    data = json.dumps(jd)
    resp = Response(data, status=200, mimetype='application/json')
    return resp