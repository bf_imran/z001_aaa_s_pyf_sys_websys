from flask import url_for


class TestTrack(object):
    def test_track_page(self, client):
        """ Track page should respond with a success 200. """
        response = client.get(url_for('track.track_form'))
        assert response.status_code == 200