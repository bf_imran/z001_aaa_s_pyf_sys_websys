from flask import Blueprint, render_template, request
from flask import Response, json
from flask import jsonify

health_blueprint = Blueprint('health', __name__, template_folder='templates')

@health_blueprint.route('/health-report')
def check_health():
    return render_template('health/report_health.html')

@health_blueprint.route('/health', methods=["GET"])
def get_health():
    jd = {'status': 'OK'}
    data = json.dumps(jd)
    resp = Response(data, status=200, mimetype='application/json')
    return resp



@health_blueprint.route("/get_ip", methods=["GET"])
def get_my_ip():
    return jsonify({'ip': request.remote_addr}), 200
