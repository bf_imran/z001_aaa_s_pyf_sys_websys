from flask import Blueprint, render_template
from flask import current_app as app
import sys

page_blueprint = Blueprint('page', __name__, template_folder='templates')

@page_blueprint.route('/')
def home():

    msg = sys.version
    return render_template('page/home.html', msg=msg)

@page_blueprint.route('/terms')
def terms():
    return render_template('page/terms.html')


@page_blueprint.route('/privacy')
def privacy():
    return render_template('page/privacy.html')
