from flask import current_app
from server_app_extensions import db
from app_library.lib_sqlalchemy import ResourceMixin, AwareDateTime

class Track(ResourceMixin,db.Model):

   __tablename__ = 'tracking'

   t_id = db.Column('track_id', db.Integer, primary_key = True)
   t_lat = db.Column(db.String(128))
   t_lon = db.Column(db.String(128))  
   t_status = db.Column(db.String(128))  
   t_status2 = db.Column(db.String(128))  

def __init__(self, t_id, t_lat, t_lon):
   self.t_id = t_id
   self.t_lat = t_lat
   self.t_lon = t_lon
   self.t_status = t_status