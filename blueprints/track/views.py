from flask import Blueprint, render_template, request

track_blueprint = Blueprint('track', __name__, template_folder='templates')

@track_blueprint.route('/track',methods = ['GET'])
def track_form():
    return render_template('track/track.html')

@track_blueprint.route('/result',methods = ['POST'])
def result():
  if request.method == 'POST':
    result = request.form
  return render_template("track/result.html",result = result)