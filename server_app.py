from flask import Flask, render_template, request
import sys
import logging

from app_library import lib_json as lj
from app_library import lib_logger as logger

from blueprints.page import page_blueprint
from blueprints.track import track_blueprint
from blueprints.health import health_blueprint

from server_app_extensions import db

app_gen_settings = lj.loadSettings("app_settings/app_general.json")

log_file = False
log_console = False
debug_mode = False

def create_app():

    global log_file,log_console,debug_mode

    app = Flask(__name__)
    app.config.from_object('app_config.config')

    log_file = app.config["LOG_TO_FILE"]
    log_console = app.config["LOG_TO_CONSOLE"]
    debug_mode = app.config["ACTIVATE_DEBUG"]

    app.register_blueprint(page_blueprint)
    app.register_blueprint(track_blueprint)
    app.register_blueprint(health_blueprint)

    extensions(app)
    create_logging(app)

    return app

def create_logging(app):

    #logger.setuplog(log_file,log_console,"app_logs/auditlog-%Y-%m-%d.txt")
    logger.setuplog(log_file,log_console,"app_logs/%Y%m%d_%I%M%S_Audit.log")
    logger.log(log_file,log_console,"Starting {} version {}".format( app_gen_settings['app_name'], app_gen_settings['app_version']))
    logger.log(log_file,log_console,"Server Started.")
    logger.log(log_file,log_console,"Access Server at > http://localhost:5000/")
    logger.log(log_file,log_console,"Access Server at > http://localhost:5000/track")
    logger.log(log_file,log_console,"Access Server at > http://localhost:5000/result")
    logger.log(log_file,log_console,"Access Server at > http://localhost:5000/health")
    logger.log(log_file,log_console,"Access Server at > http://localhost:5000/get_ip")
    logger.log(log_file,log_console,"Access Server at > http://localhost:5000/shutdown")
    return None


def extensions(app):

    db.init_app(app)

    return None


# When you directly call the server_app.py file.
if __name__ == "__main__":

    manager = create_app()
    try:
        manager.run(debug=debug_mode)
    finally:
        logger.stopmsg(log_file,log_console)
